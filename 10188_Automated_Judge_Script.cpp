// 10188 - Automated Judge Script
// 可以將每行字串全部接在一起，中間用’\n’來隔離
// 這樣AC只要比較這個接起來的字串是否相等即可
// 而PE只要將字串中非數字的字元刪掉，再看看有沒有相等即可
// 剩下的就是WA了。

#include<iostream>
#include<cstdio>
#include<cctype>
using namespace std;

int main(){
    int n, m, max_index;
    int runcase = 0;
    string answer, output;
    string input;
    bool ac, pe;

    while( scanf( "%d", &n ) != EOF && n != 0 ){
        getchar();

        answer = "";
        output = "";

        for( int i = 0 ; i < n ; i++ ){
            getline( cin, input );
            if( i ) answer += '\n', answer += input;
            else answer = input;
        }
        scanf( "%d", &m );
        getchar();
        for( int i = 0 ; i < m ; i++ ){
            getline( cin, input );
            if( i ) output += '\n', output += input;
            else output = input;
        }

        ac = true;
        if( answer != output ) ac = false;

        if( ac ){
            printf( "Run #%d: Accepted\n", ++runcase );
            continue;
        }

        // region 
        // 如果經過上面的AC檢查（字串是否完全相等）沒通過，代表字串一定不完全相等
        // 而題目只要求PE情況是數字部分不完全相等，故抹去非數字部分後，answer跟output兩字串如相等
        // 代表一定是PE（因為如果原本就都相等順序都對，就是AC了）
        pe = true;
        for( int i = 0 ; i < answer.length() ; i++ )
            if( !isdigit(answer[i]) ) answer.erase(i,1), i--;
        for( int i = 0 ; i < output.length() ; i++ )
            if( !isdigit(output[i]) ) output.erase(i,1), i--;

        max_index = max( m, n );
        if( answer != output ) pe = false;

        if( pe ){
            printf( "Run #%d: Presentation Error\n", ++runcase );
            continue;
        }
        // endregion 
        
        printf( "Run #%d: Wrong Answer\n", ++runcase );

    }

    return 0;
}