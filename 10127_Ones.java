// 10127 - Ones

import java.math.BigInteger;
import java.util.Scanner;

class Main
{
    public static void main (String args[])
    {
        Scanner scanner = new Scanner(System.in);
        
        while (scanner.hasNext())
        {
            BigInteger fraction = scanner.nextBigInteger();
            int counter = 3;  // 直接從3位數開始，因為1跟11都不可能
            String denominator_str = "111";
            BigInteger denominator = new BigInteger(denominator_str); 
            while (!denominator.mod(fraction).equals(BigInteger.ZERO)) {
                denominator=denominator.multiply(BigInteger.TEN).add(BigInteger.ONE);
                counter++;
            }
            System.out.println(counter);
        }
    }
}

