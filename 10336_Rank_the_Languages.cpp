// 10336－Rank the Languages

#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;

const int MAP_SIZE = 20;  // 地圖長or寬
const int ALPHA_SIZE = 26;  // 小寫字母總數

char map[MAP_SIZE][MAP_SIZE] = {0};
bool visited[MAP_SIZE][MAP_SIZE] = {0};  // 紀錄走過哪些路

void DFS( int x, int y ){
    visited[x][y] = 1;
    if( map[x+1][y] == map[x][y] && !visited[x+1][y] ) DFS(x+1,y);
    if( map[x-1][y] == map[x][y] && !visited[x-1][y] ) DFS(x-1,y);
    if( map[x][y+1] == map[x][y] && !visited[x][y+1] ) DFS(x,y+1);
    if( map[x][y-1] == map[x][y] && !visited[x][y-1] ) DFS(x,y-1);
}

int main(){
    int Num, Height, Width, max_count;
    while( scanf( "%d", &Num ) != EOF ){
        for( int i = 1 ; i <= Num ; i++ ){
            scanf( "%d%d", &Height, &Width );
            getchar();
            memset( visited, 0, sizeof(visited) );  // reset二維陣列
            int area_count[ALPHA_SIZE] = {0};  // 字母陣列（用來記錄使用各語言的區域數量）
            max_count = 0;  // 紀錄哪個語言（字母）具有的使用區域最多（這裡只是因為output要照大小排序)

            for( int j = 1 ; j <= Height ; j++ ){
                for( int k = 1 ; k <= Width ; k++ )
                    scanf( "%c", &map[j][k] );
                getchar();  // 接收input的換行
            }

            for( int j = 1 ; j <= Height ; j++ ){
                for( int k = 1 ; k <= Width ; k++ ){
                    if( !visited[j][k] ){
                        max_count = max( ++area_count[map[j][k]-'a'], max_count );  // 減'a'為將char轉int(存入子母陣列)
                                                                                    // 取max只是為了記錄目前最大值，因output要依序輸出
                        // 找到一個還未走過的點之後，尋訪四周使用同語言的城鎮（如找到，則也幫那些城鎮標注，變成一個區域的概念，才不會重複累加）
                        DFS( j, k );
                    }
                }
            }

            printf( "World #%d\n", i );
            // 以下照value大小排序輸出
            for( int j = max_count ; j >= 1 ; j-- ){
                for( int k = 0 ; k < ALPHA_SIZE ; k++ ){
                    if( area_count[k] == j )
                        printf( "%c: %d\n", 'a'+k, j );  // 加'a'為將int轉char
                }
            }
        }
    }
    return 0;
}