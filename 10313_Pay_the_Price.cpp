// 10313 - Pay the Price
// dp[target_price][upper_bound_coin_amount] 表示目標價格可以用多少個 “不大於” upper_bound_coin_amount數量的硬幣組合。
// 有兩種情況
// 1. 包括upper_bound_coin_amount：dp[target_price][upper_bound_coin_amount] += dp[target_price-upper_bound_coin_amount][upper_bound_coin_amount];
// 2. 不包括upper_bound_coin_amount：dp[target_price][upper_bound_coin_amount] += dp[target_price][upper_bound_coin_amount-1];


#include <cstdio>
#include <cstring>
#include <algorithm>
#include <sstream>
using namespace std;

const int N = 301;
long long dp[N][N] = {0};  // dp[target_price][upper_bound_coin_amount] 表示目標價格可以用多少個 “不大於” upper_bound_coin_amount數量的硬幣組合出來。

void init() {
    dp[0][0] = 1;  // 目標是0元的話，用0個硬幣可以達成
    for(int target_price = 0; target_price < N; target_price++) {
        for(int upper_bound_coin_amount = 1; upper_bound_coin_amount < N; upper_bound_coin_amount++) {
            // 如target_price < upper_bound_coin_amount，因最低幣值是1元，所以不可能
            if(target_price - upper_bound_coin_amount >= 0) {  
                // 用upper_bound_coin_amount數量的硬幣去組合出目標價格
                dp[target_price][upper_bound_coin_amount] += dp[target_price - upper_bound_coin_amount][upper_bound_coin_amount];
            }
            // 用小於upper_bound_coin_amount數量的硬幣（如upper_bound_coin_amount-1, upper_bound_coin_amount -2...）去組合出目標價格
            dp[target_price][upper_bound_coin_amount] += dp[target_price][upper_bound_coin_amount-1];
        }
    }
}

int main() {
    init();
    char line[N];
    int n,l1,l2;
    while(gets(line)) {
        l1 = l2 = -1;
        sscanf(line,"%d%d%d",&n,&l1,&l2);
        l1 = min(l1,300);
        l2 = min(l2,300);
        if(l1 == -1) {  // 後面接0個參數
            printf("%lld\n",dp[n][n]);
        }else if(l2 == -1) {  // 後面接1個參數
            printf("%lld\n",dp[n][l1]);
        }else {  // 後面接2個參數
            printf("%lld\n",dp[n][l2] - dp[n][l1-1]);
        }
    }
    return 0;
}
