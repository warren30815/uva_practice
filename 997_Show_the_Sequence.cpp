// 997 - Show the Sequence

#include <stdio.h> 
#include <string.h>
#include <ctype.h>
#include <iostream>

using namespace std ;

const int MAX_OUTPUT_SIZE = 50;
long long answer[128];

void parsing(char str_input[]) {
    int sign = 1;  // 正負號
    int offset, num = 0;  // 這裡的num即為題目的m
    char pos;

    // region 整理解析輸入，將數值表示存到tmp
    // i從1開始是因為，第一個char一定是'['，就沒必要處理它
    for(offset = 1; str_input[offset]; offset++) {
        if(str_input[offset] == '-')	sign = -1;
        else if(isdigit(str_input[offset]))
            num = num * 10 + str_input[offset] - '0';
        else {
            pos = str_input[offset];
            break;
        }
    }
    num = sign * num;
    // endregion

    
    if(pos == '+') {
        // 加1是因為，str_input + offset的下一個char一定是'['，就沒必要處理它
        char* next_starting_index = str_input + offset + 1;
        parsing(next_starting_index);
        // S_before_update_answer為S下標i-1；answer即為V
        long long S_before_update_answer = answer[0];  
        // 這裡answer[0]被新的m取代
        answer[0] = num;
        for(int i = 1; i < MAX_OUTPUT_SIZE; i++) {
            long long tmp = answer[i];
            answer[i] = answer[i - 1] + S_before_update_answer;
            S_before_update_answer = tmp;
        }
    } else if(pos == '*') {
        // 加1是因為，str_input + offset的下一個char一定是'['，就沒必要處理它
        char* next_starting_index = str_input + offset + 1;
        parsing(next_starting_index);
        answer[0] *= num;  // 這裡answer[0]即為V1
        for(int i = 1; i < MAX_OUTPUT_SIZE; i++)
            answer[i] = answer[i] * answer[i-1];
    } else {  // 遇到 ']' 時
        for(int i = 0; i < MAX_OUTPUT_SIZE; i++)
            answer[i] = num;
    }
}

int main() {
    char str_input[1024];
    int num_output;
    while(scanf("%s %d", str_input, &num_output) == 2) {
        memset(answer, 0, sizeof(answer));  // reset answer array
        parsing(str_input);
        for(int i = 0; i < num_output; i++)
            printf("%lld%c", answer[i], i == num_output - 1 ? '\n' : ' ');
    }
    return 0;
}