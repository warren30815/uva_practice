// 10140 - Prime Distance
// 對於區間[left,right]，如果區間中的一個數（假設為s)是合數
// 那麼他就能質因子分解成比它小的素數的積，當然s的質因子可能有多個，有的可能很大
// 但對於判斷s是合數來說，我們只需要找到它最小的質因子就可以了，而且我們能肯定的是
// 這個質因子一定小於等於sqrt(s)，由於數據的最大值為2147483647,它的sqrt(2147483647)=46341，因此我們只需要篩素數篩到46341即可，然後在對這個區間進行篩素數,即把合數給去掉就可以了
#include <cstring>
#include <cstdio>

// sqrt(2^31) = 46340.95...
const int sqrt_of_2_to_the_31_power = 46341;
const int MAX_difference = 1000001;  // 多一個1是為了陣列不要溢出

int visit[sqrt_of_2_to_the_31_power], prime[sqrt_of_2_to_the_31_power];

// 找出2~n的所有質數，並建表，回傳為質數數量
int makePrimeList(int n)
{
    memset(visit, 0, sizeof(visit));
    int prime_count = 0;
    for (int i = 2; i < n; ++ i) {
        if (!visit[i]) {
            prime[prime_count ++] = i;
            for (int j = i+i; j < n; j += i) {
                visit[j] = 1;
            }
        }
    }
    return prime_count;
}

int candidateList[MAX_difference];  // 區間內(L~U)所有可能的候選人
int ans[MAX_difference];  // 用來記錄L到U區間所有的prime

// 因為一個一個找質數會算很久，所以用刪去法優化
void calculatePrime(int prime_count, int L, int U)
{
    memset(candidateList, 1, sizeof(candidateList));

    for (int i = 0; i < prime_count; ++ i) {
        // 這行看不懂
        int s = (L-1)/prime[i] + 1;
        // s < 2的話下面乘法沒意義
        if (s < 2) s = 2;
        // j += prime[i] 就等同 (s+1)*prime[i]
        // 下面篩選掉合數（標註為0）
        for (int j = s * prime[i]; j <= U; j += prime[i]) {
            candidateList[j-L] = 0;
        }
    }

    // region 把candidateList內的資料轉換到ans array
    int ans_count = 0;
    for (int i = L; i <= U; ++ i) {
        // 檢查i > 1是因為1不是質數
        if (i > 1 && candidateList[i-L]) {
            ans[ans_count ++] = i;
        }
    }
    // endregion

    // region
    // 找出最短和最長距離的質數pair
    if (ans_count < 2)
        puts("There are no adjacent primes.");
    else {
        int max = -2147483647, min = 2147483647, minLeft = 0, minRight = 0, maxLeft = 0, maxRight = 0;
        for (int i = 1; i < ans_count; ++ i) {
            if (max < ans[i]-ans[i-1]) {
                max = ans[i]-ans[i-1];
                maxLeft = ans[i-1]; 
                maxRight = ans[i];
            }
            if (min > ans[i]-ans[i-1]) {
                min = ans[i]-ans[i-1];
                minLeft = ans[i-1]; 
                minRight = ans[i];
            }
        }
        printf("%d,%d are closest, %d,%d are most distant.\n",minLeft,minRight,maxLeft,maxRight);
    }
    // endregion
}

int main()
{
    int prime_count = makePrimeList(sqrt_of_2_to_the_31_power);
    int L, U;
    while (~scanf("%d%d",&L,&U)) {
        calculatePrime(prime_count, L, U);
    }
    return 0;
}