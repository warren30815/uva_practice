// 10137 - The Trip
// 在旅途中有的人付車錢，有的人付餐錢，或其他有的沒的。
// 在旅遊結束後，大家想分攤花費，讓每個人付的錢都能相等，但因為最小幣值是0.01元，而平均後可能會出現0.006之類的
// 所以題目容許每個人最多可以相差0.01元，問你讓每個人付的錢都盡量相等的最小金錢轉移量是多少。
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <iostream>
using namespace std;

bool cmp(int a, int b) { return a > b; }

int cost[1010];

int main()
{
     int n, avg, total, remnant, ans, t1, t2;
     while(scanf("%d", &n) && n != 0)
     {
         ans = avg = total = 0;

         // region
         // 把整數部分跟小數部分分開讀取，並把原本的float * 100變成都是int（1dollar = 100cent概念），避免float精度問題
         for(int i = 0; i < n; ++i) {
             scanf("%d.%d", &t1, &t2);
             cost[i] = t1 * 100 + t2;
             total += cost[i];
         }
         // endregion

         remnant = total % n;  // 零頭
         avg = total / n;

         // 將花最多錢的人排到最前面（概念是假設我先墊錢，我可以少拿1cent回來沒關係，但如果如以下情況（有人沒付），就不能用減1cent（少拿）的方式計算）
         // 如測資：
	        // 6
			// 0.00
			// 0.00
			// 0.00
			// 0.00
			// 0.00
			// 0.02
         sort(cost, cost + n, cmp);

         for(int i = 0; i < remnant; ++i){
         	 // 題目容許每個人最多可以相差0.01元，所以只要誤差在0.01也算對 -> 沒考慮這點的話會Wrong answer
         	 // 多減1是因為每個人可以不care那1cent，所以可以找remnant數量的人頭，每一人少拿回一cent
         	 // -> 這樣就不會有餘數了
             ans += abs(cost[i] - avg - 1);
         }
         for(int i = remnant; i < n; ++i){
             ans += abs(cost[i] - avg);
         }

         // 因為上面是先算出平均後再計算兩側差多少的絕對值，所以比平均大跟比平均小都會視為正數累加上去，故要除2平衡回來
         // 且一開始*100，所以最後算答案要除：2 * 100 = 200
     	 printf("$%.2lf\n", ans / 200.0);
     }
     return 0;
 }