// 10295 - Hay Points 
// 將單字建成表後，遇到文章就將每個字查表總和即是解答。
#include<iostream>
#include<map>
#include<cstring>
#include<cmath>

using namespace std;

int main()
{

    map<string,int> q;
    string s;
    int m,n;
    int total;
    int money;

    cin>>m>>n;

    for(int i=0;i<m;i++)
    {
	    cin>>s>>money;
	    q[s]=money;
    }   

    for(int j = 0; j < n; j++){
    	while(cin>>s && s != ".")
    	{
      		if(q[s]>0) total+=q[s];
      	}
		cout<< total << endl;
		total = 0;
    }

    return 0;
}