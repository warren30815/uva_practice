// 10126_Zipf’s Law

#include <iostream>
#include <cstdio>
#include <cctype>
#include <map>

using namespace std;

map<string,int> myMap;

// 從句子切割單詞
void getWords(string cad){
	string tmp = "";

	for(int x=0; x<cad.size(); x++)
	{
	    if(isalpha(cad[x]))
	    {
	    	tmp+=tolower(cad[x]);
	    }
	    else
	    {
        	if(tmp.size() > 0)
        	{
        		myMap[tmp]++;
        		tmp = "";
        	}
	    }

	}

	// 如有剩餘的char組成一個word輸出
	if(tmp.size() > 0){
		myMap[tmp]++;
	}
	// reset
	tmp = "";
}

int main(){
	int n;
	bool is_starting_input_data = false;  // 是否為第一筆資料輸入前（如果是，輸入第一筆資料前，要先額外輸出一個/n，題目規定）
	string cad;  // 讀入的字串

	while(scanf("%d",&n) != EOF){
	    while(getline(cin,cad)){
		    if(cad == "EndOfText")
		        break;
		    getWords(cad);
	    }
	    // 代表是否有符合條件的word
	    bool flag = false;

	    // 輸入第一筆資料前，要先額外輸出一個/n，題目規定
	    if(!is_starting_input_data){
	    	is_starting_input_data = true;
	    }
	    else{
	    	printf("\n");
	    }

	    for(auto i : myMap)
	    {
	        if(i.second == n)
	        {
	            printf("%s\n", i.first.c_str());
	            flag = true;
	        }
	    }

	    if(flag == false)
	    	printf("There is no such word.\n");
	    myMap.clear();
	}
	return 0;
}