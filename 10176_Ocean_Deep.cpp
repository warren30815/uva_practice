// 10176 - Ocean Deep - Make it shallow.cpp

#include <stdio.h>
#include <iostream>

using namespace std;

int main() {
    char s;
    long long ans = 0;
    while(s != -1) {
        s = getchar();
        if(s == '1' || s == '0') {
            ans = (ans << 1) + s - '0';  // 減'0'是為了將char轉回int
            if(ans >= 131071)
                ans %= 131071;
        } else if(s == '#') {
            puts(ans ? "NO" : "YES");
            ans = 0;
        }
    }
    return 0;
}