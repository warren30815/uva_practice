// 11286 - Conformity

// 題目是要求在學生選擇的課程組合中，找出受歡迎的課程組合，輸出選擇了受歡迎的課程組合學生人數。
// 但要注意，受歡迎的課程組合可不一定只有一個。
// 例一: 共有 A、B、C 三種組合，但都各只有一個人選，那得回報 3。
// 例二: 共有 A、B、C 三種組合，A、B各有兩個人選，C只有一個人選，要回報 4。
//           因為A和B人氣一樣高呀!

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>

using namespace std;

int main(){
	int n;
	while (cin>>n, n){
		map<set<int>,int> m;
		int tmp;
		for (int j = 0; j < n; j++){
			set<int> se;
			for (int i = 0; i < 5; i++){
				cin>>tmp;
				se.insert(tmp);
			}
			m[se] += 1;
		}
		int Max = -1, ans = 0;
		// 找出最大值（最熱門的課程組合）
		for (auto i : m)
			if (i.second > Max) Max = i.second;
		// 看多少人符合最大值（多少人是選這組合）
		for (auto i : m)
			if (i.second == Max) ans+=Max;
		cout<<ans<<endl;
	}
}