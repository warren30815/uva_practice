#include <iostream>
#include <cstdio>
#include <set>
using namespace std;

int squareSum(int n){
    int sum = 0;
    while( n > 0 ){
        sum += (n % 10) * (n % 10);
        n /= 10;
    }

    return sum;
}

int main(){
    int n;
    while( scanf("%d", &n) != EOF ){
        for( int caseCount = 1 ; caseCount <= n ; ++caseCount ){
            int N;
            scanf("%d", &N);

            set<int> checked;
            checked.insert(N);

            bool isHappy = true;
            int number = N;
            while( number != 1 ){
                // 將數字的各個位數平方之總和求出
                number = squareSum(number);
                // 若遇到之前已經求過的數字就知道是Unhappy，若能夠到1那就是Happy。
                // set中的find函數：
                // Searches the container for an element equivalent to val and returns an iterator to it if found
                // otherwise it returns an iterator to set::end.
                if( checked.find(number) != checked.end() ){
                    isHappy = false;
                    break;
                }
                checked.insert(number);
            }

            if( isHappy ){
                printf("Case #%d: %d is a Happy number.\n", caseCount, N);
            }
            else {
                printf("Case #%d: %d is an Unhappy number.\n", caseCount, N);
            }
        }
    }
    return 0;
}